unit ControleFinanceiro.Model.Entidade;

interface

uses
  Data.DB,ControleFinanceiro.Model.Factory;
  type iModelEntidade = interface
   ['{CFD23C3C-DB7F-49F8-BD49-ECF7A698CF68}']
   function Open: Boolean;
   function FDataSet: TDataSet;
   function GetDataSet: TDataSet;
   function RetornaValorColuna(coluna: string;Param:String='';Valor:String=''):string;
   function ApplyUpdates(dataSet:TDataSet):Boolean;

   procedure Close;
  end;

  type TModelEntidade = class(TInterfacedObject,iModelEntidade)
  private
    FIndex: string;
    FAutoInc: String;
    FSQL: String;
    procedure SetAutoInc(const Value: String);
    procedure SetIndex(const Value: string);
    procedure SetSQL(const Value: String);
  protected
    function FDataSet:TDataSet;virtual;abstract;
    function RetornaValorColuna(coluna: string;Param:String='';Valor:String=''):string;virtual;
  public
    function Open: Boolean;virtual;
    function ApplyUpdates(dataSet:TDataSet): Boolean;
    procedure Close;
    function GetDataSet: TDataSet;
    property SQL:String read FSQL write SetSQL;
    property AutoInc: String read FAutoInc write SetAutoInc;
    property Index:string read FIndex write SetIndex;
  end;


implementation

uses
  Vcl.Forms, Winapi.Windows;

{ TModelEntidade }
var
  factory :iDataSet;

function TModelEntidade.ApplyUpdates(dataSet:TDataSet): Boolean;
begin
  Result := factory.ApplyUpdates(dataSet);
end;

procedure TModelEntidade.Close;
begin
  factory.Close;
end;

function TModelEntidade.GetDataSet: TDataSet;
begin
  Result := factory.GetDataSet;
end;

function TModelEntidade.Open: Boolean;
begin
  factory := TFactoryQuery.New;
  factory.Open(SQL,Index,AutoInc);
  Result := True;
end;


function TModelEntidade.RetornaValorColuna(coluna: string;Param:String='';Valor:String=''):string;
begin
  Result:= factory.RetornaValorColuna(SQL,coluna,Param,Valor);
end;

procedure TModelEntidade.SetAutoInc(const Value: String);
begin
  FAutoInc := Value;
end;

procedure TModelEntidade.SetIndex(const Value: string);
begin
  FIndex := Value;
end;

procedure TModelEntidade.SetSQL(const Value: String);
begin
  FSQL := Value;
end;

end.
