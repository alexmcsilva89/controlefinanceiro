unit ControleFinanceiro.Model.LeituraOFX;

interface

uses
  ControleFinanceiro.Model.Entidade, Data.DB, ControleFinanceiro.Model.Factory,
  Datasnap.DBClient;

  type TModelLeituraOFX = class(TModelEntidade)
  public
    dataSet :TDataSource;
    class function New: TModelLeituraOFX;
    function Open: Boolean; override;
    function FDataSet: TDataSet;override;
  end;

implementation

{ TModelLeituraOFX }

function TModelLeituraOFX.FDataSet: TDataSet;
begin
  Result := dataSet.DataSet;
end;

class function TModelLeituraOFX.New: TModelLeituraOFX;
begin
  Result := TModelLeituraOFX.Create;
end;

function TModelLeituraOFX.Open: Boolean;
begin
  if not Assigned(dataSet) then
    dataSet := TDataSource.Create(nil);

  SQL     := 'select * from item_import_ofx order by movdate desc';
  AutoInc := 'Seq';
  Index   := 'MovDate';
  Result  := inherited;
  dataSet.DataSet := GetDataSet;
end;

end.
