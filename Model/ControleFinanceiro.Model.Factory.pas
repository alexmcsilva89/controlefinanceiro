unit ControleFinanceiro.Model.Factory;

interface

uses
  Data.DB, FireDAC.Comp.Client,Datasnap.DBClient,
  Datasnap.Provider,System.SysUtils, FireDAC.Stan.Option,FireDAC.Dapt,System.Classes,ControleFinanceiro.Controller.Conexao;

  type iDataSet = interface
   ['{B4AD350F-5693-466E-A7D0-E48946DF34AC}']
   function Open(SQL: String;Index: String; AutoInc: String): TDataSet;
   function Post: Boolean;
   function RetornaValorColuna(SQL: string;coluna: string;Param:String='';Valor:String=''):string;
   function ApplyUpdates(dataSet:TDataSet): Boolean;
   function GetDataSet: TDataSet;
   procedure Close;
  end;

  type TFactoryQuery = class(TInterfacedObject,iDataSet)
  private
    FDQuery: TFDQuery;
    function Open(SQL: string; Index: string; AutoInc: string): TDataSet;
    function Post: Boolean;
    function GetDataSet: TDataSet;
    function RetornaValorColuna(SQL: string;coluna: string;Param:String='';Valor:String=''):string;
    function ApplyUpdates(dataSet:TDataSet): Boolean;
  public
    class function New: TFactoryQuery;
    procedure Close;
  end;


implementation

uses
  Vcl.Forms, Winapi.Windows;


{ TFactoryQuery }

function TFactoryQuery.ApplyUpdates(dataSet:TDataSet): Boolean;
begin
  try
    Result := TFDQuery(dataSet).ApplyUpdates(0)=0;
  except on E:Exception do
    Application.MessageBox(PChar('Falha na grava��o no banco de dados! Erro:'+E.Message+'!'), 'Aviso', MB_OK + MB_ICONWARNING);
  end;
end;

procedure TFactoryQuery.Close;
begin
  FDQuery.Close;
  FreeAndNil(FDQuery);
end;

function TFactoryQuery.GetDataSet: TDataSet;
begin
  Result := FDQuery;
end;

class function TFactoryQuery.New: TFactoryQuery;
begin
  Result := TFactoryQuery.Create;
end;

function TFactoryQuery.Open(SQL, Index, AutoInc: string): TDataSet;
begin
  if not Assigned(FDQuery) then
    FDQuery := TFDQuery.Create(nil);

  with FDQuery do
  begin
    CachedUpdates                   := True;
    IndexFieldNames                 := Index;
    Connection                      := TFDConnection(TControllerConexao.New.GetConexao);
    UpdateOptions.AssignedValues    := [uvGeneratorName];
    UpdateOptions.AutoIncFields     := AutoInc;
    UpdateOptions.AutoCommitUpdates := True;
    FetchOptions.RowsetSize         := 1000;
    FetchOptions.Mode               := fmOnDemand;
  end;
  FDQuery.SQL.Text                  := SQL;
  FDQuery.FieldList.Create(FDQuery);
  FDQuery.Active := True;

  Result := FDQuery;
end;

function TFactoryQuery.Post: Boolean;
begin
 try
   FDQuery.Post;
   Result := True;
 except
   Result := False;
 end;
end;

function TFactoryQuery.RetornaValorColuna(SQL: string;coluna: string;Param:String='';Valor:String=''):string;
var
  Query: TFDQuery;
begin
  try
    Result := '';
    Query := TFDQuery.Create(nil);
    Query.Connection := TFDConnection(TControllerConexao.New.GetConexao);
    Query.SQL.Text   := SQL;

    if Param <> emptystr then
      Query.ParamByName(Param).Value := Valor;
    Query.Open;
    Result := Query.FieldByName(Coluna).AsString;
  finally
    FreeAndNil(Query);
  end;
end;

end.
