unit ControleFinanceiro.Model.Conexao;

interface
uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client,FireDAC.Comp.UI,FireDAC.Phys.MSSQLDef,
  FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,IniFiles, IWSystem;


type iModelConexao = interface
['{FE875284-BB48-4A8A-8150-B5F636B6AB48}']
  function Open: Boolean;
  function Close: Boolean;
  function GetConexao: TCustomConnection;
end;

type TModelConexao = class(TInterfacedObject,iModelConexao)
private
  FConn: TFDConnection;
public
  function Open: Boolean;
  function Close: Boolean;
  function GetConexao: TCustomConnection;
  class function New: iModelConexao;
end;

type TFuncoes = class
private
  class function LerIni(Chave1, Chave2: String; ValorPadrao: String = ''): String; static;
  class procedure GravarIni;static;
end;


implementation

uses
  Vcl.Forms, Winapi.Windows;

{ TModelConnFiredac }
function TModelConexao.Close: Boolean;
begin
  try
    if FConn.Connected then
        FConn.Connected := False;
    FreeAndNil(FConn);
    Result := True;
  except on E:Exception do
    begin
      Application.MessageBox(PChar('Ocorreu uma falha ao desconectar do banco de dados: ' + E.Message), 'Aviso', MB_OK + MB_ICONWARNING);
      Result := False;
    end;
  end;
end;

function TModelConexao.GetConexao: TCustomConnection;
begin
  Result := FConn;
end;

class function TModelConexao.New: iModelConexao;
begin
  Result := TModelConexao.Create;
end;

function TModelConexao.Open: Boolean;
begin
  if not Assigned(FConn) then
    FConn := TFDConnection.Create(nil);

  try
    TFuncoes.GravarIni;
    with FConn do
    begin
      Params.Clear;
      Params.Values['DriverID']  := 'MSSQL';
      Params.Values['Server']    := TFuncoes.LerIni('SQLSERVER','Server');
      Params.Values['Database']  := TFuncoes.LerIni('SQLSERVER','Database');
      Params.Values['User_Name'] := TFuncoes.LerIni('SQLSERVER','User');
      Params.Values['Password']  := TFuncoes.LerIni('SQLSERVER','Password');
      LoginPrompt := False;
      Connected := True;
      Result := True;
    end;
  except on E:Exception do
    begin
      Application.MessageBox(PChar('Ocorreu uma falha ao conectar no banco de dados: ' + E.Message), 'Aviso', MB_OK + MB_ICONWARNING);
      Result := False;
    end;
  end;
end;

{ TFuncoes }
class procedure TFuncoes.GravarIni;
var
  Arquivo: String;
  FileIni: TIniFile;
begin
  Arquivo := gsAppPath + gsAppName + '.ini';
  try
    FileIni := TIniFile.Create(Arquivo);
    if not FileExists(Arquivo) then
    begin
      FileIni.WriteString('SQLSERVER','Server','DESKTOP-EMP77ND\SQLEXPRESS');
      FileIni.WriteString('SQLSERVER','User','administrador');
      FileIni.WriteString('SQLSERVER','Password','250919');
      FileIni.WriteString('SQLSERVER','Database','controlefinanceiro');
    end;
  finally
    FreeAndNil(FileIni)
  end;
end;

class function TFuncoes.LerIni(Chave1, Chave2, ValorPadrao: String): String;
var
  Arquivo: String;
  FileIni: TIniFile;
begin
  Arquivo := gsAppPath + gsAppName + '.ini';
  result := ValorPadrao;
  try
    FileIni := TIniFile.Create(Arquivo);
    if FileExists(Arquivo) then
      result := FileIni.ReadString(Chave1, Chave2, ValorPadrao);
  finally
    FreeAndNil(FileIni)
  end;
end;

end.
