unit ControleFinanceiro.Model.LeituraHeaderOFX;

interface

uses
  ControleFinanceiro.Model.Entidade, Data.DB, Datasnap.DBClient;

type TModelLeituraHeaderOFX = class(TModelEntidade)
  public
    dataSource:TDataSource;
    class function New: TModelLeituraHeaderOFX;
    function Open: Boolean; override;
    function RetornaValorColuna(coluna: string; Param: string = '';
      Valor: string = ''): string; override;
    function FDataSet: TDataSet;override;
end;

implementation

{ TModelLeituraHeaderOFX }
function TModelLeituraHeaderOFX.FDataSet: TDataSet;
begin
  result := dataSource.DataSet;
end;

class function TModelLeituraHeaderOFX.New: TModelLeituraHeaderOFX;
begin
  Result := TModelLeituraHeaderOFX.Create;
end;

function TModelLeituraHeaderOFX.Open: Boolean;
begin
  dataSource := TDataSource.Create(nil);
  SQL     := 'select * from head_import_ofx';
  AutoInc := 'Seq';
  Index   := 'BankID';
  Result  := inherited;
  dataSource.DataSet := GetDataSet;
end;

function TModelLeituraHeaderOFX.RetornaValorColuna(coluna, Param,
  Valor: string): string;
begin
  SQL    := 'select * from head_import_ofx where fileimport =:'+Param;
  Result := inherited
end;

end.
