unit ControleFinanceiro.Model.ImportaCotacao;

interface

uses
  ControleFinanceiro.Model.Entidade, Data.DB;

  type  TModelImportaCotacao = class(TModelEntidade)

  public
    dataSource : TDataSource;
    class function New: TModelImportaCotacao;
    function Open: Boolean; override;
    function FDataSet: TDataSet;override;
    function RetornaValorColuna(coluna: string;Param:String='';Valor:String=''):string;override;
  end;

implementation

{ TModelImportaCotacao }

function TModelImportaCotacao.FDataSet: TDataSet;
begin
  result := dataSource.DataSet;
end;

class function TModelImportaCotacao.New: TModelImportaCotacao;
begin
  result := TModelImportaCotacao.Create;
end;

function TModelImportaCotacao.Open: Boolean;
begin
  dataSource := TDataSource.Create(nil);
  SQL     := 'select * from cotacao_moeda';
  AutoInc := 'SEQ';
  Index   := 'SEQ';
  Result  := inherited;
  dataSource.DataSet := GetDataSet;
end;

function TModelImportaCotacao.RetornaValorColuna(coluna: string;Param:String='';Valor:String=''):string;
begin
  SQL    := 'select top (1)* from cotacao_moeda where moeda =:'+Param+' order by datacotacao desc';
  Result := inherited;
end;

end.
