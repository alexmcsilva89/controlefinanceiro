unit ControleFinanceiro.Controller.Entidade;

interface

uses
  Data.DB;
  type iDataSet = interface
   ['{CFD23C3C-DB7F-49F8-BD49-ECF7A698CF68}']
   function Open: TDataset;
   function Insert: Boolean;
   function Delete: Boolean;
   function Edit: Boolean;
   procedure Close;
  end;


implementation

end.
