program ControleFinanceiro;

uses
  Vcl.Forms,
  ControleFinanceiro.View.ConsultaAplicacao in 'View\ControleFinanceiro.View.ConsultaAplicacao.pas' {frmConsultaAplicacao},
  ControleFinanceiro.Model.Conexao in 'Model\ControleFinanceiro.Model.Conexao.pas',
  ControleFinanceiro.Controller.Conexao in 'Controller\ControleFinanceiro.Controller.Conexao.pas',
  ControleFinanceiro.Controller.OFX in 'Controller\ControleFinanceiro.Controller.OFX.pas',
  ControleFinanceiro.Controller.LeituraOFX in 'Controller\ControleFinanceiro.Controller.LeituraOFX.pas',
  ControleFinanceiro.Model.LeituraOFX in 'Model\ControleFinanceiro.Model.LeituraOFX.pas',
  ControleFinanceiro.Model.Factory in 'Model\ControleFinanceiro.Model.Factory.pas',
  ControleFinanceiro.Model.Entidade in 'Model\ControleFinanceiro.Model.Entidade.pas',
  ControleFinanceiro.Model.LeituraHeaderOFX in 'Model\ControleFinanceiro.Model.LeituraHeaderOFX.pas',
  ControleFinanceiro.Controller.Cotacao in 'Controller\ControleFinanceiro.Controller.Cotacao.pas',
  ControleFinanceiro.Controller.ImportaCotacao in 'Controller\ControleFinanceiro.Controller.ImportaCotacao.pas',
  ControleFinanceiro.Model.ImportaCotacao in 'Model\ControleFinanceiro.Model.ImportaCotacao.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmConsultaAplicacao, frmConsultaAplicacao);
  Application.Run;
end.
