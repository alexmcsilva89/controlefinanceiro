unit ControleFinanceiro.View.ConsultaAplicacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Data.DB,
  Vcl.Grids, Vcl.DBGrids, ControleFinanceiro.Controller.Conexao,
  ControleFinanceiro.Controller.LeituraOFX,
  ControleFinanceiro.Controller.Cotacao,
  ControleFinanceiro.Controller.ImportaCotacao;

type
  TfrmConsultaAplicacao = class(TForm)
    pnPrincipal: TPanel;
    pnCotacao: TPanel;
    pnBitcoin: TPanel;
    shBitcoin: TShape;
    pnDolar: TPanel;
    spDolar: TShape;
    pnEuro: TPanel;
    spEuro: TShape;
    pnAtualizar: TPanel;
    lblDataCotacao: TLabel;
    lblCotacoes: TLabel;
    btnAtualizar: TButton;
    pnShapePrincipal: TPanel;
    shPrincipal: TShape;
    pnImportar: TPanel;
    lblValorSaldo: TLabel;
    lblSaldo: TLabel;
    btnImportar: TButton;
    pnCabecalho: TPanel;
    lblMovimentacaoBancaria: TLabel;
    pnGrid: TPanel;
    gridMovimentacao: TDBGrid;
    lblDolar: TLabel;
    lblVendaD: TLabel;
    lblDolarVenda: TLabel;
    lblCompraD: TLabel;
    lblDolarCompra: TLabel;
    lblVariacaoD: TLabel;
    lblDolarVariacao: TLabel;
    lblEuro: TLabel;
    lblVendaE: TLabel;
    lblEuroVenda: TLabel;
    lblCompraE: TLabel;
    lblEuroCompra: TLabel;
    lblVariacaoE: TLabel;
    lblEuroVariacao: TLabel;
    lblBitcoin: TLabel;
    lblVendaB: TLabel;
    lblBitcoinVenda: TLabel;
    lblCompraB: TLabel;
    lblBitcoinCompra: TLabel;
    lblVariacaoB: TLabel;
    lblBitcoinVariacao: TLabel;
    dsImportacao: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnImportarClick(Sender: TObject);
    procedure gridMovimentacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAtualizarClick(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaColunas;
    procedure PreencheCotacao;
  public
    { Public declarations }
  end;

var
  frmConsultaAplicacao: TfrmConsultaAplicacao;
  controllerConexao: iControllerConexao;
  controllerLeituraOFX : TControllerLeituraOFX;
  controllerCotacao: TImportaCotacao;

implementation
{$R *.dfm}

procedure TfrmConsultaAplicacao.btnAtualizarClick(Sender: TObject);
begin
  controllerCotacao.Open;
  controllerCotacao.AtualizaCotacao;
  PreencheCotacao;
end;

procedure TfrmConsultaAplicacao.btnImportarClick(Sender: TObject);
begin
  dsImportacao.DataSet := controllerLeituraOFX.ImportarArquivoOFX;
  lblValorSaldo.Caption := 'R$ '+controllerLeituraOFX.RetornaSaldo('INITIALBALANCE','FILEIMPORT');
  HabilitaColunas;
end;

procedure TfrmConsultaAplicacao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  controllerConexao.Close;
  FreeAndNil(controllerLeituraOFX);
  frmConsultaAplicacao := nil;
  action := caFree;
end;

procedure TfrmConsultaAplicacao.FormCreate(Sender: TObject);
begin
  controllerConexao    := TControllerConexao.New;
  controllerConexao.Open;
  controllerLeituraOFX := TControllerLeituraOFX.New;
  controllerCotacao    := TImportaCotacao.New;
end;

procedure TfrmConsultaAplicacao.gridMovimentacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = 46) then
  Key := 0;
end;

procedure TfrmConsultaAplicacao.HabilitaColunas;
begin
  gridMovimentacao.Columns[0].FieldName := 'MovDate';
  gridMovimentacao.Columns[1].FieldName := 'Desc';
  gridMovimentacao.Columns[2].FieldName := 'Value';
end;

procedure TfrmConsultaAplicacao.PreencheCotacao;
begin
  with controllerCotacao do
  begin
    lblDataCotacao.Caption   := FormatDateTime('dd/mm/yyyy',Now);
    lblDolarVenda.Caption    := RetornaValorColunaCotacao('VENDAVALOR','MOEDA','DOLLAR');
    lblDolarCompra.Caption   := RetornaValorColunaCotacao('COMPRAVALOR','MOEDA','DOLLAR');
    lblDolarVariacao.Caption := RetornaValorColunaCotacao('VARIACAOVALOR','MOEDA','DOLLAR');

    lblEuroVenda.Caption     := RetornaValorColunaCotacao('VENDAVALOR','MOEDA','EURO');
    lblEuroCompra.Caption    := RetornaValorColunaCotacao('COMPRAVALOR','MOEDA','EURO');
    lblEuroVariacao.Caption  := RetornaValorColunaCotacao('VARIACAOVALOR','MOEDA','EURO');

    lblBitcoinVenda.Caption    := RetornaValorColunaCotacao('VENDAVALOR','MOEDA','BITCOIN');
    lblBitcoinCompra.Caption   := RetornaValorColunaCotacao('COMPRAVALOR','MOEDA','BITCOIN');
    lblBitcoinVariacao.Caption := RetornaValorColunaCotacao('VARIACAOVALOR','MOEDA','BITCOIN');
  end;
end;

end.
