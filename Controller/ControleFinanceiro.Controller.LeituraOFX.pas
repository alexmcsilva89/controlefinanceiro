unit ControleFinanceiro.Controller.LeituraOFX;

interface

uses
   ControleFinanceiro.Controller.OFX, Data.DB,
   Vcl.Dialogs;

type  TControllerLeituraOFX = class(TOFXReader)
public
  class function New: TControllerLeituraOFX;
  function ImportarArquivoOFX: TDataSet;
  function RetornaSaldo(coluna: string; param: string; valor: string=''): string;
end;

implementation

uses
  ControleFinanceiro.Model.LeituraOFX, ControleFinanceiro.Model.Entidade,
  ControleFinanceiro.Model.LeituraHeaderOFX, Vcl.Forms, Winapi.Windows,
  System.SysUtils;
var
  modelLeituraOFX: iModelEntidade;
  modelLeituraHeaderOFX: iModelEntidade;

{ TLeituraOFX }
function TControllerLeituraOFX.ImportarArquivoOFX: TDataSet;
var
  dialog: TOpenDialog;
  I:Integer;
begin
  try
    Result := nil;
    dialog := TOpenDialog.Create(nil);
    dialog.DefaultExt := '.OFX';
    modelLeituraOFX := TModelLeituraOFX.New;
    modelLeituraHeaderOFX := TModelLeituraHeaderOFX.New;
    if (dialog.Execute) then
    begin
      modelLeituraHeaderOFX.Open;
      modelLeituraOFX.Open;

      OFXFile := dialog.FileName;
      if UpperCase(ExtractFileExt(OFXFile)) <> '.OFX' then
      begin
        Application.MessageBox(PChar('Arquivo inv�lido!'), 'Aviso', MB_OK + MB_ICONWARNING);
        Exit;
      end;

      if not modelLeituraHeaderOFX.FDataSet.Locate('FILEIMPORT',OFXFile,[loPartialKey,locaseinsensitive]) then
      begin
        Process;
        with modelLeituraHeaderOFX do
        begin
          FDataSet.Insert;
          FDataSet.FieldByName('BANKID').AsInteger        := BankID;
          FDataSet.FieldByName('ACCOUNTID').AsString      := AccountID;
          FDataSet.FieldByName('ACCOUNTTYPE').AsString    := AccountType;
          FDataSet.FieldByName('FINALBALANCE').AsFloat    := FinalBalance;
          FDataSet.FieldByName('INITIALBALANCE').AsFloat  := InitialBalance;
          FDataSet.FieldByName('FILEIMPORT').AsString := OFXFile;
          FDataSet.Post;
          ApplyUpdates(FDataSet);
        end;
        with modelLeituraOFX do
        begin
          for I := 0 to Count - 1 do
          begin
            if not (FDataSet.Locate('ID',Get(I).ID,[loPartialKey,loCaseInsensitive])) then
            begin
              FDataSet.Insert;
              FDataSet.FieldByName('ID').AsInteger        := StrToInt(Get(I).ID);
              FDataSet.FieldByName('DESC').AsString       := Get(I).Desc;
              FDataSet.FieldByName('DOCUMENT').AsString   := Get(I).Document;
              FDataSet.FieldByName('VALUE').AsFloat       := Get(I).Value;
              FDataSet.FieldByName('MOVDATE').AsDateTime  := Get(I).MovDate;
              FDataSet.FieldByName('MOVTYPE').AsString    := Get(I).MovType;
              FDataSet.FieldByName('BANKID').AsInteger    := BankID;
              FDataSet.FieldByName('ACCOUNTID').AsString  := AccountID;
              FDataSet.Post;
            end
            else
            begin
              FDataSet.Edit;
              if Get(I).MovType = 'CREDIT'then
                FDataSet.FieldByName('VALUE').Value := GetDataSet.FieldByName('VALUE').AsFloat + Get(I).Value
              else
                FDataSet.FieldByName('VALUE').Value := GetDataSet.FieldByName('VALUE').AsFloat + Get(I).Value;
              FDataSet.FieldByName('MOVDATE').AsDateTime := Get(I).MovDate;
              FDataSet.Post;
            end;
          end;

        end;
        modelLeituraOFX.ApplyUpdates(modelLeituraOFX.FDataSet);
        Application.MessageBox(PChar('Importa��o conclu�da com sucesso!'), 'Aviso', MB_OK + MB_ICONINFORMATION);
        Result := modelLeituraOFX.FDataSet;
      end
      else begin
        Application.MessageBox(PChar('Arquivo j� foi importado!'), 'Aviso', MB_OK + MB_ICONWARNING);
        modelLeituraOFX.Open;
        Result := modelLeituraOFX.FDataSet;
      end;

    end;
  finally
    FreeAndNil(dialog);
  end;
end;

class function TControllerLeituraOFX.New: TControllerLeituraOFX;
begin
  Result := TControllerLeituraOFX.Create;
end;

function TControllerLeituraOFX.RetornaSaldo(coluna: string; param: string; valor: string=''): string;
begin
  if not Assigned(modelLeituraHeaderOFX) then
  begin
    modelLeituraHeaderOFX := TModelLeituraHeaderOFX.New;
    modelLeituraHeaderOFX.Open;
  end;
  Result := modelLeituraHeaderOFX.RetornaValorColuna(coluna,param,OFXFile);
end;

end.
