unit ControleFinanceiro.Controller.Conexao;

interface

uses
  ControleFinanceiro.Model.Conexao, Data.DB;
  type
    iControllerConexao = interface
    ['{44C993A7-8FF8-4BDD-A276-482D92932C22}']
    function Open: Boolean;
    function GetConexao: TCustomConnection;
    procedure Close;
  end;

  type TControllerConexao = class(TInterfacedObject,iControllerConexao)
  public
    function Open: Boolean;
    function GetConexao: TCustomConnection;
    procedure Close;
    class function New: iControllerConexao;
  end;

implementation

{ TControllerConexao }
var
  conn: iModelConexao;
  controllerConn: TControllerConexao;

procedure TControllerConexao.Close;
begin
  if Assigned(conn) then
    conn.Close;
end;

function TControllerConexao.GetConexao: TCustomConnection;
begin
  Result := conn.GetConexao;
end;

class function TControllerConexao.New: iControllerConexao;
begin
  if not Assigned(controllerConn)then
    controllerConn := TControllerConexao.Create;
  Result := controllerConn;
end;

function TControllerConexao.Open: Boolean;
begin
  if not Assigned(conn) then
    conn := TModelConexao.New;
  Result := conn.Open;
end;

end.
