unit ControleFinanceiro.Controller.ImportaCotacao;

interface

uses
  ControleFinanceiro.Controller.Cotacao, Data.DB,  ControleFinanceiro.Model.Entidade;

  type TImportaCotacao = class(TRetornoCotacao)
  public
    function Open: Boolean;
    function AtualizaCotacao: Boolean;
    function RetornaValorColunaCotacao(coluna: string;Param:String='';Valor:String=''):string;
    class function New: TImportaCotacao;
  end;

implementation

uses
  ControleFinanceiro.Model.ImportaCotacao, System.SysUtils;

{ TImportaCotacao }
var
  modelImportaCotacao: iModelEntidade;

function TImportaCotacao.AtualizaCotacao: Boolean;
var
  I:Integer;
  dataAtual:TDate;
begin
  if RetornoValorCotacaoJSON then
  begin
    dataAtual := Now;
    for I := 0 to listaMoedas.Count -1 do
    begin
      with modelImportaCotacao do
      begin
        if UpperCase(listaMoedas.Items[I].nome) = 'DOLLAR' then
        begin
          if (UpperCase(RetornaValorColuna('COMPRAVALOR','MOEDA','DOLLAR')) <> listaMoedas.Items[I].compra) or
             (UpperCase(RetornaValorColuna('VENDAVALOR','MOEDA','DOLLAR')) <> listaMoedas.Items[I].venda) or
             (UpperCase(RetornaValorColuna('VARIACAOVALOR','MOEDA','DOLLAR')) <> listaMoedas.Items[I].variacao)then
          begin
            FDataSet.Append;
            FDataSet.FieldByName('MOEDA').AsString := listaMoedas.Items[i].nome;
            FDataSet.FieldByName('COMPRAVALOR').AsString   := listaMoedas.Items[i].compra;
            FDataSet.FieldByName('VENDAVALOR').AsString    := listaMoedas.Items[i].venda;
            FDataSet.FieldByName('VARIACAOVALOR').AsString := listaMoedas.Items[i].variacao;
            FDataSet.FieldByName('DATACOTACAO').AsDateTime := dataAtual;
            FDataSet.Post;
            ApplyUpdates(FDataSet);
          end;
        end
        else if UpperCase(listaMoedas.Items[I].nome) = 'EURO' then
        begin
          if (UpperCase(RetornaValorColuna('COMPRAVALOR','MOEDA','EURO')) <> listaMoedas.Items[I].compra) or
             (UpperCase(RetornaValorColuna('VENDAVALOR','MOEDA','EURO')) <> listaMoedas.Items[I].venda) or
             (UpperCase(RetornaValorColuna('VARIACAOVALOR','MOEDA','EURO')) <> listaMoedas.Items[I].variacao)then
          begin
            FDataSet.Append;
            FDataSet.FieldByName('MOEDA').AsString := listaMoedas.Items[i].nome;
            FDataSet.FieldByName('COMPRAVALOR').AsString   := listaMoedas.Items[i].compra;
            FDataSet.FieldByName('VENDAVALOR').AsString    := listaMoedas.Items[i].venda;
            FDataSet.FieldByName('VARIACAOVALOR').AsString := listaMoedas.Items[i].variacao;
            FDataSet.FieldByName('DATACOTACAO').AsDateTime := dataAtual;
            FDataSet.Post;
            ApplyUpdates(FDataSet);
          end;
        end
        else if UpperCase(listaMoedas.Items[I].nome) = 'BITCOIN' then
        begin
          if (UpperCase(RetornaValorColuna('COMPRAVALOR','MOEDA','BITCOIN')) <> listaMoedas.Items[I].compra) or
             (UpperCase(RetornaValorColuna('VENDAVALOR','MOEDA','BITCOIN')) <> listaMoedas.Items[I].venda) or
             (UpperCase(RetornaValorColuna('VARIACAOVALOR','MOEDA','BITCOIN')) <> listaMoedas.Items[I].variacao)then
          begin
            FDataSet.Append;
            FDataSet.FieldByName('MOEDA').AsString := listaMoedas.Items[i].nome;
            FDataSet.FieldByName('COMPRAVALOR').AsString   := listaMoedas.Items[i].compra;
            FDataSet.FieldByName('VENDAVALOR').AsString    := listaMoedas.Items[i].venda;
            FDataSet.FieldByName('VARIACAOVALOR').AsString := listaMoedas.Items[i].variacao;
            FDataSet.FieldByName('DATACOTACAO').AsDateTime := dataAtual;
            FDataSet.Post;
            ApplyUpdates(FDataSet);
          end;
        end;
      end;
    end;
    Result := True;
  end
  else
    Result := False;
end;

class function TImportaCotacao.New: TImportaCotacao;
begin
  Result := TImportaCotacao.Create;
end;

function TImportaCotacao.Open: Boolean;
begin
  if not Assigned(modelImportaCotacao) then
     modelImportaCotacao := TModelImportaCotacao.New;
  modelImportaCotacao.Open;
  Result:= True;
end;

function TImportaCotacao.RetornaValorColunaCotacao(coluna: string;Param:String='';Valor:String=''):string;
begin
  Result := modelImportaCotacao.RetornaValorColuna(coluna,Param,Valor);
end;

end.
