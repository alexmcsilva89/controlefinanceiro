unit ControleFinanceiro.Controller.Cotacao;

interface
uses
  System.JSON, Vcl.Forms, System.SysUtils, Winapi.Windows,REST.Client,REST.Types,REST.Json,REST.Utils,
  Data.DB,IPPeerCommon,IPPeerClient,  System.Classes, System.Generics.Collections;

type TMoeda = class
public
  nome:string;
  venda:string;
  compra:string;
  variacao:string;
  class function New:TMoeda;
end;

type TDolar = class(TMoeda);
type TBitCoin = class(TMoeda);
type TEuro = class(TMoeda);


type TRetornoCotacao = class
protected
  dolar: TMoeda;
  bitCoin: TMoeda;
  Euro: TMoeda;
  listaMoedas: TList<TMoeda>;
  function RetornoValorCotacaoJSON: Boolean;
  destructor destroy;
end;


implementation

{ iMoeda }
class function TMoeda.New: TMoeda;
begin
  result := TMoeda.Create;
end;

{ TRetornoCotacao }
destructor TRetornoCotacao.destroy;
begin
  if Assigned(dolar)   then FreeAndNil(dolar);
  if Assigned(euro)    then FreeAndNil(euro);
  if Assigned(bitcoin) then FreeAndNil(bitcoin);
  if Assigned(listaMoedas) then FreeAndNil(listaMoedas);
  inherited
end;

function TRetornoCotacao.RetornoValorCotacaoJSON: Boolean;
var
  request: TRestRequest;
  client: TRestClient;
  objetoJson: TJSONObject;
  subobjetoJson: TJSONObject;
  jv: TJSONValue;
begin
  try
    try
      Result := False;
      if not Assigned(listaMoedas) then
        listaMoedas := TList<TMoeda>.Create
      else listaMoedas.Clear;

      dolar   := TDolar.New;
      euro    := TEuro.New;
      bitCoin := TBitCoin.New;

      client := TRESTClient.Create(nil);
      client.BaseURL := 'https://api.hgbrasil.com/finance';

      request := TRestRequest.Create(nil);
      request.Client := client;
      request.Execute;

      objetoJson := request.Response.JSONValue as TJSONObject;

      jv := objetoJson.Get('results').JsonValue;
      objetoJson := jv as TJSONObject;

      jv := objetoJson.Get('currencies').JsonValue;
      objetoJson       := jv as TJSONObject;
      subobjetoJson    := jv as TJSONObject;

      jv := objetoJson.Get('USD').JsonValue;
      objetoJson := jv as TJSONObject;

      dolar.nome     := objetoJson.GetValue('name').Value;
      dolar.venda    := objetoJson.GetValue('sell').Value;
      dolar.compra   := objetoJson.GetValue('buy').Value;
      dolar.variacao := objetoJson.GetValue('variation').Value;
      listaMoedas.Add(dolar);

      jv := subobjetoJson.Get('EUR').JsonValue;
      objetoJson := jv as TJSONObject;

      euro.nome     := objetoJson.GetValue('name').Value;
      euro.venda    := objetoJson.GetValue('sell').Value;
      euro.compra   := objetoJson.GetValue('buy').Value;
      euro.variacao := objetoJson.GetValue('variation').Value;
      listaMoedas.Add(euro);

      jv := subobjetoJson.Get('BTC').JsonValue;
      objetoJson := jv as TJSONObject;

      bitcoin.nome     := objetoJson.GetValue('name').Value;
      bitcoin.venda    := objetoJson.GetValue('sell').Value;
      bitcoin.compra   := objetoJson.GetValue('buy').Value;
      bitcoin.variacao := objetoJson.GetValue('variation').Value;
      listaMoedas.Add(bitcoin);
      Application.MessageBox(PChar('Atualiza��o das cota��es conclu�da!'), 'Aviso', MB_OK + MB_ICONINFORMATION);
      Result := True;
    finally
      if Assigned(client)  then client.Free;
      if Assigned(request) then request.Free;
    end;
  except
    Application.MessageBox(PChar('Falha ao consultar Cota��o!'), 'Aviso', MB_OK + MB_ICONWARNING);
  end;
end;

end.
